//***************************************************************************
//
//  Author(s)...: 
//  Target(s)...: mega16
//
//  Compiler....: IAR
//
//  Description.: ������� ��� ������ � DS1307
//
//  Data........: 25.10.13
//
//***************************************************************************
#include "rtc.h"

#define F_I2C          100000UL
#define F_CPU		   8000000UL
#define TWBR_VALUE    (((F_CPU)/(F_I2C)-16)/2)

#if ((TWBR_VALUE > 255) || (TWBR_VALUE == 0))
   #error "TWBR value is not correct"
#endif

void RTC_Init(void)
{
  TWBR = TWBR_VALUE;
  TWSR = 0;
}


void RTC_SetValue(uint8_t adr, uint8_t data)
{
  /*��������� ��������� �����*/ 
  TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
  while(!(TWCR & (1<<TWINT)));

  /*�������� ���� ����� SLA-W*/  
  TWDR = (DS1307_ADR<<1)|0;
  TWCR = (1<<TWINT)|(1<<TWEN); 
  while(!(TWCR & (1<<TWINT)));
  
  /*�������� ����� �������� ds1307*/
  TWDR = adr;
  TWCR = (1<<TWINT)|(1<<TWEN); 
  while(!(TWCR & (1<<TWINT)));
  
  /*�������� ������ ��� ����������*/
  if (data != RTC_RESET_POINTER){
     /*��� ����� �������� ������ � BCD �������*/
     data = ((data/10)<<4) + data%10; 
     
     TWDR = data;
     TWCR = (1<<TWINT)|(1<<TWEN); 
     while(!(TWCR & (1<<TWINT)));
  }
  
  /*��������� ��������� ����*/ 
  TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);  
}

uint8_t RTC_GetValue(void)
{
  uint8_t data;
  
  /*��������� ��������� �����*/
  TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
  while(!(TWCR & (1<<TWINT))); 
  
  /*�������� ���� ����� SLA-R*/
  TWDR = (DS1307_ADR<<1)|1;
  TWCR = (1<<TWINT)|(1<<TWEN); 
  while(!(TWCR & (1<<TWINT)));  
  
  /*��������� ������*/
  TWCR = (1<<TWINT)|(1<<TWEN);
  while(!(TWCR & (1<<TWINT)));
  data = TWDR;
  
  /*��������� ��������� ����*/
  TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
  
  return data; 
}

