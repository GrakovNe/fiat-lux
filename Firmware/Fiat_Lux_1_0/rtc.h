//***************************************************************************
//
//  Author(s)...: ����� ������  http://ChipEnable.Ru   
//
//  Target(s)...: mega16
//
//  Compiler....: IAR
//
//  Description.: �������� ������� ������ � DS1307
//
//  Data........: 25.10.13
//
//***************************************************************************
#ifndef RTC_H
#define RTC_H

#include <avr/io.h>
#include <stdint.h>

#define RTC_RESET_POINTER   0xff
#define DS1307_ADR  104

/*������ ���������*/

#define RTC_SEC_ADR     0x00
#define RTC_MIN_ADR     0x01
#define RTC_HOUR_ADR    0x02

/*���������������� �������*/

void RTC_Init(void);
void RTC_SetValue(uint8_t adr, uint8_t data);
uint8_t RTC_GetValue(void);

#endif //RTC_H

