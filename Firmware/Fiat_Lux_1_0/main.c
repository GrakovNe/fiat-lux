/*
* Fiat_Lux_1_0.c
*
* Created: 06.11.2015 19:49:41
* Author : GrakovNe
*/


#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include "rtc.h"


#define LED_POWER         PINB4
#define LED_POWER_PORT    PORTB
#define DOWN_BTN          PINB0
#define OK_BTN            PINB1
#define UP_BTN            PINB2
#define BTN_PIN           PINB
#define BTN_DOWN_PRESSED (~BTN_PIN&(1<<DOWN_BTN))
#define BTN_OK_PRESSED   (~BTN_PIN&(1<<OK_BTN))
#define BTN_UP_PRESSED   (~BTN_PIN&(1<<UP_BTN))


#define PWM_OUT PORTD6

#define RESET PORTC3

#define BT_CONFIG PORTD2
#define CONNECTION_LED PORTD5

#define COMMAND_PACKAGE 'C'
#define DATA_PACKAGE    'D'

#define MINIMAL_LIGHT_LEVEL 15

#define EEPROM_SYSTEM_SETTINGS_ADDRESS   (void*)0
#define EEPROM_LIGHT_SETTINGS_ADDRESS    (void*)8
#define EEPROM_ALARM_SETTINGS_ADDRESS    (void*)16
#define EEPROM_TIMER_SETTINGS_ADDRESS    (void*)39
#define EEPROM_AUTO_SETTINGS_ADDRESS     (void*)63
#define EEPROM_SYSTEM_TIME_ADDRESS       (void*)78

struct {
	unsigned char hours;
	unsigned char mins;
	unsigned char secs;
} sysTime;

struct {
	char level;
	char isActive;
	char changeRate;
} lightSettings;

struct {
	int btPasswd;
} systemSettings;

struct {
	char hours;
	char mins;
	char isActive;
	char mode;
	int  softSpeed;
} alarmSettings;

struct {
	char mins;
	char seconds;
	char isActive;
} timerSettings;

struct {
	char autoOnIsActive;
	char autoOnHours;
	char autoOnMins;
	char autoOffIsActive;
	char autoOffHours;
	char autoOffMins;
} autoSettings;

char isLocked;


inline char getChar();
inline void getTime();
void init();
void uartSendChar(char Symbol);
void uartSendPackage(char packageType, char *String);
void alarm();
void timer();
void dropLight(char state);
void gSysTime();
void reset();
void sendBTPasswd(int data);
void processPackage(char packageType,char *package);
void pollButtons();
char comparePackages(char* receivedPackage, char* samplePackage);
char* makePackagefromDig(int dig);
char* uartReceivePackage();
int makeDigFromPackage(char* package);

char currentLightLevel = 0;
char taskMode = 0;
char alarmSoftModeAllowed = 1;

/*
/ 0 - normal lamp mode when it can be turned on or off
/ 1 - alarm mode when it can be dropped into normal mode
/ 2 - timer mode when it can be dropped into normal mode
*/

ISR (USART_RX_vect){
	if (UDR0 == 'S'){
		uartReceivePackage();
	}
}

ISR(TIMER1_COMPA_vect){
	static int alarmAllowed = 1;
	static int autoOffAllowed = 1;
	static int autoOnAllowed = 1;
	TCNT1H=0x00;
	TCNT1L=0x00;
	getTime();

	if ( (alarmSettings.isActive) && (alarmAllowed) && (alarmSettings.hours == sysTime.hours) && (alarmSettings.mins == sysTime.mins) ) {
		taskMode = 1;
		alarmAllowed = 0;
	}
	
	if (timerSettings.isActive){
		
		if ( (timerSettings.seconds == 0) && (timerSettings.mins >0) ){
			timerSettings.mins --;
			timerSettings.seconds = 60;
		}
		timerSettings.seconds --;
		
		if ( (timerSettings.mins == 0) && (timerSettings.seconds == 0)) {
			taskMode = 2;
			timerSettings.isActive = 0;
		}
	}

	if ((alarmSettings.mins != sysTime.mins)&&(taskMode != 1)){
		alarmAllowed = 1;
		alarmSoftModeAllowed = 1;
	}
	
	if ( (autoSettings.autoOnIsActive) && (sysTime.hours == autoSettings.autoOnHours) && (sysTime.mins == autoSettings.autoOnMins) && (autoOnAllowed) ) {
		autoOnAllowed = 0;
		lightSettings.isActive = 1;
	}
	
	if (sysTime.mins != autoSettings.autoOnMins){
		autoOnAllowed = 1;
	}
	
	if ( (autoSettings.autoOffIsActive) && (sysTime.hours == autoSettings.autoOffHours) && (sysTime.mins == autoSettings.autoOffMins) && (autoOffAllowed) ) {
		autoOffAllowed = 0;
		lightSettings.isActive = 0;
	}
	
	if (sysTime.mins != autoSettings.autoOffMins){
		autoOffAllowed = 1;
	}
	
}

ISR (TIMER2_COMPA_vect){
	OCR2A=lightSettings.changeRate;
	TCNT2 = 0x00;
	if (OCR0A == currentLightLevel){
		return;
	}
	
	if (OCR0A > currentLightLevel){
		OCR0A --;
	}
	else {
		OCR0A ++;
	}
	
	if (OCR0A > MINIMAL_LIGHT_LEVEL){
		LED_POWER_PORT |= (1 << LED_POWER);
		return;
	}
	LED_POWER_PORT &= ~(1 << LED_POWER);
}

ISR (PCINT0_vect){
	if ((BTN_DOWN_PRESSED)||(BTN_OK_PRESSED)||(BTN_UP_PRESSED)) {
		pollButtons();
	}
}

inline char getChar(){
	while(!( UCSR0A & ( 1 << RXC0 )));
	return UDR0;
}

void uartSendChar(char Symbol){
	while(!( UCSR0A & (1 << UDRE0)));
	UDR0 = Symbol;
}

void uartSendPackage(char packageType, char *String){
	char controlSum = 0x00;
	
	uartSendChar('S');
	uartSendChar(packageType);
	
	for (int i = 0; i < 8; i++){
		uartSendChar(String[i]);
		controlSum ^= String[i];
	}
	
	uartSendChar(controlSum);
	uartSendChar('F');
	
	uartSendChar(0x0D);
	uartSendChar(0x0A);
}

char* uartReceivePackage(){
	static char packageBuffer[] = "00000000";
	char packageType = 0;
	char controlSum = 0x00;
	
	char typeBuffer = getChar();
	
	switch (typeBuffer){
		case 'C':
		packageType = COMMAND_PACKAGE;
		break;
		case 'D':
		packageType = DATA_PACKAGE;
		break;
	}
	
	for (int i = 0; i < 8; i++){
		packageBuffer[i] = getChar();
		controlSum ^= packageBuffer[i];
	}
	
	/*if (getChar() != controlSum){
	uartSendPackage(COMMAND_PACKAGE, "000ERROR");
	return;
	}*/
	
	if (getChar() != '0'){
		uartSendPackage(COMMAND_PACKAGE, "000ERROR");
		return 0;
	}
	
	
	if (getChar() != 'F'){
		uartSendPackage(COMMAND_PACKAGE, "000ERROR");
		return 0;
	}
	
	if (packageType == COMMAND_PACKAGE){
		processPackage(packageType, packageBuffer);
		return 0;
	}
	
	if (packageType == DATA_PACKAGE){
		return packageBuffer;
	}
	
	uartSendPackage(COMMAND_PACKAGE, "0000FAIL");
	reset();
	return 0;
}

void dropLight(char state){
	lightSettings.level = state;
	OCR0A = state;
}

char comparePackages(char* receivedPackage, char* samplePackage){
	for (int i = 7; i >= 0; i--){
		if (receivedPackage[i]!=samplePackage[i]){
			return 0;
		}
	}
	
	return 1;
}

int receiveDataPackage(){
	uartSendPackage(COMMAND_PACKAGE, "0000WAIT");
	if (getChar() != 'S'){
		uartSendPackage(COMMAND_PACKAGE, "000ERROR");
		return -1;
	}
	int result =  makeDigFromPackage(uartReceivePackage());
	uartSendPackage(COMMAND_PACKAGE, "000000OK");
	return result;
}

void processPackage(char packageType,char *package){
	if (comparePackages(package, "000RESET")){
		reset();
	}
	
	if (comparePackages(package, "GLAMPSTA")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(lightSettings.isActive));
		return;
	}
	
	if (comparePackages(package, "GCHARATE")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(lightSettings.changeRate));
		return;
	}
	

	if (comparePackages(package, "00GLEVEL")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(lightSettings.level));
		return;
	}
	
	if (comparePackages(package, "0GSYSTIM")){
		getTime();
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(sysTime.hours));
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(sysTime.mins));
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(sysTime.secs));
		return;
	}
	
	if (comparePackages(package, "0GBTPASS")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(systemSettings.btPasswd));
		return;
	}
	
	if (comparePackages(package, "0GALMTIM")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(alarmSettings.hours));
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(alarmSettings.mins));
		return;
	}
	
	if (comparePackages(package, "0GALMSTA")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(alarmSettings.isActive));
		return;
	}
	
	if (comparePackages(package, "GALMMODE")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(alarmSettings.mode));
		return;
	}
	
	if (comparePackages(package, "GALMSSPD")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(alarmSettings.softSpeed));
		return;
	}
	
	if (comparePackages(package, "0GTMRSTA")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(timerSettings.isActive));
		return;
	}
	
	if (comparePackages(package, "0GTMRTIM")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(timerSettings.mins));
		return;
	}
	
	if (comparePackages(package, "0GAONSTA")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(autoSettings.autoOnIsActive));
		return;
	}
	
	if (comparePackages(package, "0GAONTIM")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(autoSettings.autoOnHours));
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(autoSettings.autoOnMins));
		return;
	}
	
	if (comparePackages(package, "GAOFFSTA")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(autoSettings.autoOffIsActive));
		return;
	}
	
	if (comparePackages(package, "GAOFFTIM")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(autoSettings.autoOffHours));
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(autoSettings.autoOffMins));
		return;
	}
	
	if (comparePackages(package, "GLOCKSTA")){
		uartSendPackage(DATA_PACKAGE, makePackagefromDig(isLocked));
		return;
	}
	
	int receivedValue;
	
	if (comparePackages(package, "SLAMPSTA")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			lightSettings.isActive = receivedValue;
			eeprom_write_block((const void*)&lightSettings, EEPROM_LIGHT_SETTINGS_ADDRESS, sizeof(lightSettings));
		}
		
		return;
	}
	
	if (comparePackages(package, "00SLEVEL")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			lightSettings.level = receivedValue;
			eeprom_write_block((const void*)&lightSettings, EEPROM_LIGHT_SETTINGS_ADDRESS, sizeof(lightSettings));
		}
		return;
	}
	
	if (comparePackages(package, "SCHARATE")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			lightSettings.changeRate = receivedValue;
			eeprom_write_block((const void*)&lightSettings, EEPROM_LIGHT_SETTINGS_ADDRESS, sizeof(lightSettings));
		}
		return;
	}
	
	if (comparePackages(package, "0SSYSTIM")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			sysTime.hours = receivedValue;
			RTC_SetValue(RTC_HOUR_ADR, sysTime.hours);
		}
		
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			sysTime.mins = receivedValue;
			RTC_SetValue(RTC_MIN_ADR, sysTime.mins);
			
		}
		
		sysTime.secs = 0;
		RTC_SetValue(RTC_SEC_ADR, 0);
		return;
	}
	
	if (comparePackages(package, "0SBTPASS")){
		int btReceivedValue = receiveDataPackage();
		if (btReceivedValue >= 0){
			systemSettings.btPasswd = btReceivedValue;
			eeprom_write_block((const void*)&systemSettings, EEPROM_SYSTEM_SETTINGS_ADDRESS, sizeof(systemSettings));
		}
		
		return;
	}
	
	if (comparePackages(package, "0SALMTIM")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			alarmSettings.hours = receivedValue;
			eeprom_write_block((const void*)&alarmSettings, EEPROM_ALARM_SETTINGS_ADDRESS, sizeof(alarmSettings));
		}
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			alarmSettings.mins = receivedValue;
			eeprom_write_block((const void*)&alarmSettings, EEPROM_ALARM_SETTINGS_ADDRESS, sizeof(alarmSettings));
		}
		
		sysTime.secs = 0;
		
		return;
	}
	
	if (comparePackages(package, "0SALMSTA")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			alarmSettings.isActive = receivedValue;
			eeprom_write_block((const void*)&alarmSettings, EEPROM_ALARM_SETTINGS_ADDRESS, sizeof(alarmSettings));
		}
		
		return;
	}
	
	if (comparePackages(package, "SALMMODE")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			alarmSettings.mode = receivedValue;
			eeprom_write_block((const void*)&alarmSettings, EEPROM_ALARM_SETTINGS_ADDRESS, sizeof(alarmSettings));
		}
		
		return;
	}
	
	if (comparePackages(package, "SALMSSPD")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			alarmSettings.softSpeed = receivedValue;
			eeprom_write_block((const void*)&alarmSettings, EEPROM_ALARM_SETTINGS_ADDRESS, sizeof(alarmSettings));
		}
		
		return;
	}
	
	if (comparePackages(package, "0STMRSTA")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			timerSettings.isActive = receivedValue;
			eeprom_write_block((const void*)&timerSettings, EEPROM_TIMER_SETTINGS_ADDRESS, sizeof(systemSettings));
			
		}
		
		return;
	}
	
	if (comparePackages(package, "0STMRTIM")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			timerSettings.mins = receivedValue;
			timerSettings.seconds = 0;
			eeprom_write_block((const void*)&timerSettings, EEPROM_TIMER_SETTINGS_ADDRESS, sizeof(systemSettings));
			
		}
		return;
	}
	
	if (comparePackages(package, "0SAONSTA")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			autoSettings.autoOnIsActive = receivedValue;
			eeprom_write_block((const void*)&autoSettings, EEPROM_AUTO_SETTINGS_ADDRESS, sizeof(autoSettings));
		}
		return;
	}
	
	if (comparePackages(package, "0SAONTIM")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			autoSettings.autoOnHours = receivedValue;
			eeprom_write_block((const void*)&autoSettings, EEPROM_AUTO_SETTINGS_ADDRESS, sizeof(autoSettings));
		}
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			autoSettings.autoOnMins = receivedValue;
			eeprom_write_block((const void*)&autoSettings, EEPROM_AUTO_SETTINGS_ADDRESS, sizeof(autoSettings));
		}
		return;
	}
	
	if (comparePackages(package, "SAOFFSTA")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			autoSettings.autoOffIsActive = receivedValue;
			eeprom_write_block((const void*)&autoSettings, EEPROM_AUTO_SETTINGS_ADDRESS, sizeof(autoSettings));
		}
		return;
	}
	
	if (comparePackages(package, "SAOFFTIM")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			autoSettings.autoOffHours = receivedValue;
			eeprom_write_block((const void*)&autoSettings, EEPROM_AUTO_SETTINGS_ADDRESS, sizeof(autoSettings));
		}
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			autoSettings.autoOffHours = receivedValue;
			eeprom_write_block((const void*)&autoSettings, EEPROM_AUTO_SETTINGS_ADDRESS, sizeof(autoSettings));
		}
		
		return;
	}
	
	if (comparePackages(package, "SLOCKSTA")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			isLocked = receivedValue;
			eeprom_write_block((const void*)&systemSettings, EEPROM_SYSTEM_SETTINGS_ADDRESS, sizeof(systemSettings));
		}
		return;
	}
	
	if (comparePackages(package, "0000DROP")){
		receivedValue = receiveDataPackage();
		if (receivedValue >= 0){
			dropLight(receivedValue);
		}
		return;
	}
	
	uartSendPackage(COMMAND_PACKAGE, "00FAIL");
}

char* makePackagefromDig(int dig) {
	static char packageBuffer[8] = "0000000";
	for (int i = 0; i < 8; i++) {
		packageBuffer[7 - i] = (dig % 10) + 0x30;
		dig = dig / 10;
	}
	return packageBuffer;
}

int makeDigFromPackage(char* package){
	static int dig = 0;
	dig = 0;
	dig += (package[7] - 0x30);
	dig += (package[6] - 0x30) * 10;
	dig += (package[5] - 0x30) * 100;
	dig += (package[4] - 0x30) * 1000;
	dig += (package[3] - 0x30) * 10000;
	return dig;
}

inline void getTime(){
	RTC_SetValue(0, RTC_RESET_POINTER);
	unsigned char Time_Temp;
	Time_Temp = RTC_GetValue();
	sysTime.secs = (Time_Temp>>4)*10;
	sysTime.secs += Time_Temp &0xf;
	
	Time_Temp = RTC_GetValue();
	sysTime.mins = (Time_Temp>>4)*10;
	sysTime.mins += Time_Temp &0xf;
	
	Time_Temp = RTC_GetValue();
	sysTime.hours = (Time_Temp>>4)*10;
	sysTime.hours += Time_Temp &0xf;
}

void reset(){
	uartSendPackage(COMMAND_PACKAGE, "000000OK");
	PORTC &= ~(1<<RESET);
}

void pollButtons(){
	switch (taskMode){
		case 0:
		if (isLocked){
			return;
		}
		while ((BTN_DOWN_PRESSED)&&(lightSettings.isActive)) {
			if (lightSettings.level > 0){
				lightSettings.level --;
				OCR0A --;
				if (OCR0A < MINIMAL_LIGHT_LEVEL){
					LED_POWER_PORT &= ~(1 << LED_POWER);
				}
				_delay_ms(15);
			}
			continue;
		}
		
		while ((BTN_UP_PRESSED)&&(lightSettings.isActive)) {
			if (lightSettings.level < 254){
				lightSettings.level ++;
				OCR0A ++;
				if (OCR0A > MINIMAL_LIGHT_LEVEL){
					LED_POWER_PORT |= (1 << LED_POWER);
				}
				_delay_ms(15);
			}
			continue;
		}
		
		if (BTN_OK_PRESSED) {
			lightSettings.isActive = !lightSettings.isActive;
			if ((lightSettings.isActive)&&(lightSettings.level < MINIMAL_LIGHT_LEVEL)){
				OCR0A = MINIMAL_LIGHT_LEVEL+1;
				lightSettings.level = MINIMAL_LIGHT_LEVEL+1;
			}
			
			while (BTN_OK_PRESSED);
		}
		
		eeprom_write_block((const void*)&lightSettings, EEPROM_LIGHT_SETTINGS_ADDRESS, sizeof(lightSettings));
		break;
		
	case 1: /*for alarm breaking*/
	case 2: /*for timer breaking*/
		taskMode = 0;
		break;
		
	}
}

void init(){
	
	CLKPR=0x80;
	CLKPR=0x00;
	/*IO initialization (MCU)*/
	DDRB  |= (1 << LED_POWER);
	PORTB |= (1 << DOWN_BTN) | (1 << OK_BTN) | (1 << UP_BTN);
	DDRD  |= (1 << CONNECTION_LED) | (1 << BT_CONFIG) | (1 << PWM_OUT);
	PORTD |= (1 << CONNECTION_LED);
	PORTC |= (1 << RESET);
	DDRC  |= (1 << RESET);
	
	/*UART initialization (dialog)*/
	UCSR0A=0x00;
	UCSR0B=0x98;
	UCSR0C=0x06;
	UBRR0H=0x00;
	UBRR0L=0x0C;
	
	/*Timer 0 initialization (PWM)*/
	
	TCCR0A=0x83;
	TCCR0B=0x04;
	TCNT0=0x00;
	OCR0A=0x01;
	OCR0B=0x00;
	
	/*Timer 1 initialization (Light mode and system clock)*/
	
	TCCR1A=0x00;
	TCCR1B=0x05;
	TCNT1H=0x00;
	TCNT1L=0x00;
	ICR1H=0x00;
	ICR1L=0x00;
	OCR1AH=0x1E;
	OCR1AL=0x85;
	OCR1BH=0x00;
	OCR1BL=0x00;
	TIMSK1=0x02;
	
	/*Timer 2 initialization (automatic light changing)*/
	ASSR=0x00;
	TCCR2A=0x00;
	TCCR2B=0x07;
	TCNT2=0x00;
	OCR2A=0xFF;
	OCR2B=0x00;
	TIMSK2=0x02;
	
	/*PCINT initialization (buttons)*/
	
	EICRA=0x00;
	EIMSK=0x00;
	PCICR=0x01;
	PCMSK0=0x07;
	PCIFR=0x01;
	
	/*RTC initialization*/

	RTC_Init();
	
	/*Settings initialization (reading from EEPROM)*/
	
	eeprom_read_block(&lightSettings, EEPROM_LIGHT_SETTINGS_ADDRESS, sizeof(lightSettings));
	eeprom_read_block(&systemSettings, EEPROM_SYSTEM_SETTINGS_ADDRESS, sizeof(systemSettings));
	eeprom_read_block(&autoSettings, EEPROM_AUTO_SETTINGS_ADDRESS, sizeof(autoSettings));
	eeprom_read_block(&alarmSettings, EEPROM_ALARM_SETTINGS_ADDRESS, sizeof(alarmSettings));
	eeprom_read_block(&timerSettings, EEPROM_TIMER_SETTINGS_ADDRESS, sizeof(timerSettings));


	/*BT module initialization*/
	
	sendBTPasswd(systemSettings.btPasswd);
	
	uartSendPackage(DATA_PACKAGE, "00BOOTOK");
	
	sei();
}

void lightBlinker(){
	static char direction = 0;
	if ((direction < 255)&&(direction == 0)){
		currentLightLevel =255;
		if (OCR0A == 255){
			direction = 1;
		}
	}
	
	if ((direction > 0)&&(direction == 1)){
		currentLightLevel = 0;
		if (OCR0A == 0){
			direction = 0;
		}
	}
}

void sendBTPasswd(int data){
	PORTD |= (1 << BT_CONFIG);
	
	_delay_ms(1);
	
	char configString [] = "AT+PSWD=";
	for (int i = 0; i < sizeof(configString); i++){
		uartSendChar(configString[i]);
	}
	
	char *dataAsString = makePackagefromDig(data);
	
	for (int i = 4; i < 8; i++){
		uartSendChar(dataAsString[i]);
	}
	
	uartSendChar(0x0D);
	uartSendChar(0x0A);
	
	PORTD &= ~(1 << BT_CONFIG);
}

void alarm(){
	if ((alarmSettings.mode)&&(currentLightLevel < 255)&&(alarmSoftModeAllowed)){
		static int preCounter = 0;
		if(preCounter == alarmSettings.softSpeed){
			currentLightLevel++;
			preCounter = 0;
		}
		preCounter ++;
		return;
	}
	
	alarmSoftModeAllowed = 0;
	
	lightBlinker();
	
}

void timer(){
	lightBlinker();
}

int main(void)
{
	init();

	taskMode = 0;
	
	while (1)
	{
		switch (taskMode){
			case 0:
				if (lightSettings.isActive){
					currentLightLevel = lightSettings.level;
				}
				else {
					currentLightLevel = 0;
				}
			break;
			
			case 1:
				alarm();
			break;
			
			case 2:
				timer();
			break;
		}
		_delay_ms(1);
	}
}

